import * as ol from 'ol';
import * as olLayer from 'ol/layer';
import * as olSource from 'ol/source';
import * as olGeom from 'ol/geom';
import {Style, Fill, Stroke} from 'ol/style/';
import HexBin from 'ol-ext/source/HexBin';  // ol.source.HexBin = source.Vector + ol.hexGrid

let exampleMapExtent = [2056328.081581274, 7231726.245072734, 2093285.384757157, 7250185.787403604];
let exampleMapCenter = [2074806.7331692155, 7240956.016238169];

function createLayer (type, options) {
  if (!type || (typeof type) !== "string") {
    console.warn('Wrong or missing layer type parameter: ' + type);
    return null
  } else {
    let defaultName;
    switch (type) {
      case 'OSM':
        defaultName = 'OpenStreetMap';
        return new olLayer.Tile({
            name: options && options.name ? options.name : defaultName,
            title: options && options.switcher == true ? (options.name ? options.name : defaultName) : null,
            source: new olSource.XYZ({
              url: options && options.url ? options.url : 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
            })
          })
      case 'WMS': //not working atm
        defaultName = 'WMS';
        return new olLayer.Image({
            name: options && options.name ? options.name : defaultName,
            title: options && options.switcher == true ? (options.name ? options.name : defaultName) : null,
            extent: exampleMapExtent,
            source: new olSource.ImageWMS({
              url: options && options.url ? options.url : 'http://mapy.geoportal.gov.pl/wss/service/img/guest/ORTO/MapServer/WMSServer',
              ratio: options && options.ratio ? options.ratio : 1
            })
          })
      case 'Hex':
        defaultName = 'Hexagon Layer';
        let src = new HexBin({
          source: new olSource.Vector(),
          size: options && options.hexSize ? options.hexSize : 1000,
          extent: exampleMapExtent,
          layout: 'flat'
        });
        addRandomHexFeatures(src.getSource(), 10, 2000)
        return new olLayer.Vector({
            title: options && options.switcher == true ? (options.name ? options.name : defaultName) : null,
        		name: options && options.name ? options.name : defaultName,
            renderMode: options && options.raster == true ? 'image' : 'vector',
            source: src,
            style: options && options.style ? vectorStyles[options.style] : vectorStyles['black']
          })
      default:
        console.warn('Wrong layer type: ' + type);
        return null;
      }
  }
}

let createVectorStyle = (type, config) => {
  if (!type || (typeof type) !== "string") {
    console.warn("Missing or wrong type of type for createVectorStyle function: " + typeof type);
    return;
  }
  let params = {};
  switch (type) {
    case 'line':
      let stroke = {};
      config.color ? stroke.color = config.color : null;
      config.width ? stroke.width = config.width : null;
      params.stroke = new Stroke(stroke);
      break;
    default:
      console.warn("No such layer style in vectorStyles object: " + type);
  }
  return new Style(params);
}

let vectorStyles = {
  'black': createVectorStyle('line', {
    color: 'black'
  }),
  'blue': createVectorStyle('line', {
    color: 'blue',
    width: 2
  }),
  'red': createVectorStyle('line', {
    color: 'red',
    width: 3
  }),
  'green': createVectorStyle('line', {
    color: 'green'
  }),
  'blueFill': new Style({
      fill: new Fill({
        color: 'blue'
      }),
      stroke: new Stroke({
        color: 'black'
      })
  }),
  'redFill': new Style({
    fill: new Fill({
      color: 'red'
    }),
    stroke: new Stroke({
      color: 'black'
    })
  })
}

let addRandomHexFeatures = (source, hexSize, hexQuantity) => { // change to generateHexFeatures(source, hexSize)
	var ext = exampleMapExtent;
	var dx = ext[2] - ext[0];
	var dy = ext[3] - ext[1];
	var dl = Math.min(dx,dy);
	var features = [];
	for (var i=0; i < hexQuantity/hexSize; ++i)	{
    var seed = [ext[0]+dx*Math.random(), ext[1]+dy*Math.random()]
      var f = new ol.Feature(
        new olGeom.Point([
          seed[0] + dl/10*Math.random(),
    			seed[1] + dl/10*Math.random()
        ])
      );
			f.set('id', i);
			features.push(f);
	}
	source.clear();
	source.addFeatures(features);
};

export {createLayer, createVectorStyle};
