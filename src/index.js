import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './components/App.jsx';

if (typeof window !== 'undefined') {
    window.React = React;
    window.ReactDOM = ReactDOM;
}

ReactDOM.render(<App/>, document.getElementById('appMainContainer'));
