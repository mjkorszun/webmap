import React from 'react';
import ReactDOM from 'react-dom';
import {MapContainer} from './Map.jsx';
import styles from '../css/app.css';


export class App extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <MapContainer/>
    )
  }
}
