import React from 'react';
import {Map, View} from 'ol';
import * as Interaction from 'ol/interaction';
import Overlay from 'ol/Overlay';
import * as Control from 'ol/control';
import * as Proj from 'ol/proj';
import Hover from 'ol-ext/interaction/Hover';
import {createLayer, createVectorStyle} from '../LayerManager';
import LayerSwitcher from 'ol-layerswitcher';
import styles from '../../node_modules/ol-layerswitcher/src/ol-layerswitcher.css';

let MapObject;
let getLayerByName = (name) => {
	let found = null;
	MapObject.getLayers().getArray().forEach(
		(layer) => {
			if (layer.getProperties().name && layer.getProperties().name == name) {
				found = layer;
			}
		}
	)
	!found ? console.warn("Layer not found by name: " + name) : null;
	return found;
}

let LayersArray = [
	createLayer('OSM'),
	createLayer('Hex', {
		name: 'New small hexagons'
	}),
	createLayer('Hex', {
		name: 'Green hexagons',
		raster: true, //add renderBuffer
		hexSize: 4000,
		style: 'green',
		switcher: true
	}),
	createLayer('Hex', {
		name: 'Blue hexagons',
		raster: true, //add renderBuffer
		hexSize: 5000,
		style: 'blue',
		switcher: true
	}),
	createLayer('Hex', {
		name: 'Red hexagons',
		raster: true, //add renderBuffer
		hexSize: 6000,
		style: 'red',
		switcher: true
	})
];
LayersArray = LayersArray.filter((layer) => {return layer}); //cleaning up nulls

var interactions = {
	hover: new Hover({ cursor: "pointer" })
}

interactions.hover.on("enter", function(e)
	{
		displayTooltip(e);
	}
);

var tooltip;
var overlay = new Overlay({
  element: tooltip,
  offset: [10, 0],
  positioning: 'bottom-left'
});

function displayTooltip(evt) {
  var pixel = evt.pixel;
  var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
    return feature;
  });

	tooltip = document.getElementById('tooltip');
  tooltip.style.display = feature ? '' : 'none';
  if (feature) {
    overlay.setPosition(evt.coordinate);
		tooltip.innerHTML = "feature: " + feature.ol_uid;
  }
};

let MapConfig = {
  target: "mapContainer",
  layers: LayersArray,
  view: new View({
    center: Proj.fromLonLat([18.638306, 54.372158]),
    zoom: 12
  }),
  controls: Control.defaults({
      attribution: false,
      zoom: false,
      rotate: false
  })
};

var layerSwitcher = new LayerSwitcher({
		tipLabel: 'Legend' // Optional label for button
});


export class MapContainer extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {
    MapObject = new Map(MapConfig);
		MapObject.addControl(layerSwitcher);
		MapObject.addInteraction(interactions.hover);
		MapObject.addOverlay(overlay);
    window.map = MapObject; //testing purposes
  }

  render() {
    return (
      <div id="mapContainer">
				<div id="tooltip">TEST</div>
			</div>
    )
  }
}
